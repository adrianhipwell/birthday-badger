# Birthday Badger

The interactive birthday message for that badger closest to you!

## Getting Started

To make this birthday message work you will need the following items:

- Raspberry Pi
- SD card
- LED matrix
- Development computer

## Raspberry Pi Setup

- Download latest Raspbian Lite [image](https://downloads.raspberrypi.org/raspbian_lite_latest)
- Install [Etcher](https://www.balena.io/etcher/)
  - Linux: Download & install
  - Windows: `choco install -y etcher`
- Put the SD card into your development computer
- Use Etcher to write Rasbian Lite to SD card
- Copy the files *./other/wpa_supplicant.conf* and *./other/ssh* to the **boot** volume on the SD card
- Edit *wpa_supplicant.conf* with your WiFi SSID and password
- Put the SD card in the Raspberry Pi and power it on
- SSH into the Raspberry Pi from your development computer using the command `ssh pi@raspberrypi.home` with the password *raspberry*
- Copy your public SSH key (contents of *~/.ssh/id_rsa.pub*) to a new line in the Raspberry Pi *~/.ssh/authorized_keys* file (use `touch ~/.ssh/authorized_keys` if it does not exist)
- Change the default Raspberry Pi password with the command `passwd`
- Reconnect with SSH to make sure that the SSH key works correctly

## Connecting the LED Matrix

The LED matrix needs to be connected to the correct Raspberry Pi pins (*see Appendix A*) to work correctly.


- Connect the jumper cables to the LED Matrix
- Connect the jhumper cables to the Raspberry Pi
  - Connect **VCC** to **Pin 2**
  - Connect **GND** to **Pin 6**
  - Connect **DIN** to **Pin 19**
  - Connect **CS** to **Pin 24**
  - Connect **CLK** to **Pin 23**

## Running this Code

- Clone this repository down to your development computer and make sure that the [.NET Core 2.2 SDK](https://dotnet.microsoft.com/download) is installed
- Check that your Raspberry Pi has been set up as above, has the LED matrix connected and is powered on
- Build and deploy the code
  - Linux: `bash ./scripts/push-to-pi`
  - Windows: `./scripts/push-to-pi.ps1`
- ENJOY the show!

## Appendix

### Appendix A

Raspberry Pi Header

![Pi Header](https://i.stack.imgur.com/KL4PZ.png)
