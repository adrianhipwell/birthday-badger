﻿using System;
using System.Device.Spi;
using System.Device.Spi.Drivers;
using System.Threading;
using Iot.Device.Max7219;

namespace BirthdayBadger
{
  class Program
  {
    private static readonly byte[] _smiley = new byte[] {
                    0b00111100,
                    0b01000010,
                    0b10100101,
                    0b10000001,
                    0b10100101,
                    0b10011001,
                    0b01000010,
                    0b00111100
                };

    private static readonly byte[] _heart = new byte[] {
                    0b00000000,
                    0b00100010,
                    0b01110111,
                    0b01111111,
                    0b01111111,
                    0b00111110,
                    0b00011100,
                    0b00001000
                  };

    static void Main(string[] args)
    {
      Console.WriteLine("Start!");

      var connectionSettings = new SpiConnectionSettings(0, 0)
      {
        ClockFrequency = 10_000_000, // Max7219.SpiClockFrequency,
        Mode = System.Device.Spi.SpiMode.Mode0 // Max7219.SpiMode
      };
      var spi = new UnixSpiDevice(connectionSettings);
      using (var devices = new Max7219(spi, cascadedDevices: 4))
      {
        DrawRotatingIcon(devices, _smiley);

        DrawBannerText(devices, "Happy birthday Badger!", 1);

        DrawRotatingIcon(devices, _heart);

        Console.WriteLine("Fin!");
        devices.ClearAll();
      }
    }

    private static void DrawRotatingIcon(Max7219 devices, byte[] icon)
    {
      Console.WriteLine("Drawing icon...");
      devices.Init();

      // Write a smiley to devices buffer
      for (var i = 0; i < devices.CascadedDevices; i++)
      {
        for (var digit = 0; digit < 8; digit++)
        {
          devices[i, digit] = icon[digit];
        }
      }

      var rotations = new RotationType[] {
        RotationType.None,
        RotationType.Left,
        RotationType.Half,
        RotationType.Right,
        RotationType.None
      };

      foreach (RotationType rotation in rotations)
      {
        devices.Rotation = rotation;
        devices.Flush();
        Thread.Sleep(500);
      }
    }

    private static void DrawBannerText(Max7219 devices, string text, int repeat)
    {
      Console.WriteLine("Drawing banner...");
      devices.Init();

      devices.Rotation = RotationType.Right;
      var graphics = new MatrixGraphics(devices, Fonts.LCD);
      for (int i = 1; i <= repeat; i++)
      {
        graphics.ShowMessage(text, alwaysScroll: true);
      }
    }
  }
}
